# PM-Test

## Requirements

1. Design high level requirements for app/apps (you can propose how many of them are needed and what exactly - mobile. web and etc) to be able to post and manage sale advertisements, the best reference might be Avito eco-system. Please design one user story  as detailed as possible for any  feature (for example posting new advertisement).  It should be detailed enough for SCRUM team to deliver.

2. Please provide your best guess about possible estimate taking into account that you have a team of 1 front end dev, 1 mobile dev, 1 backend dev, 1 designer, 1 QA and you as a PM.

3. Please describe roughly what the workflow of the development will be.

4. Please list top 5 questions that you would ask from stakeholder taking into account that what is written in item 1 is the only information you get

## Comments

Result of work can be sent as link to shared google doc. 
